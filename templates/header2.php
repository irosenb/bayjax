<!--
	Isaac Rosenberg
	header2.php
	Laying out the topbar, which includes the route form. 
-->
</head>
<body onload="initialize()">
	<div class='navbar navbar-fixed-top'>
	  <div class='navbar-inner'>
	    <div class='container'>
	      <a class='btn btn-navbar' data-toggle='collapse' data-target='.nav-collapse'>
	        <span class='icon-bar'></span>
	        <span class='icon-bar'></span>
	        <span class='icon-bar'></span>
	      </a>
	      <a class='brand' href='index.php'>BayJAX</a>
	      
	      <form id="form" class="form-inline pull-right" onsubmit="return request();" style="margin: 0;">
			<input type="hidden"><select type="text" name="route" id="route">
				<option id="route">Pittsburg/Bay Point - SFIA/Millbrae</option>
				<option id="route">Fremont - Richmond</option>
				<option id="route">Fremont - Daly City</option>
				<option id="route">Richmond - Daly City/Millbrae</option>
				<option id="route">Dublin/Pleasanton - Daly City</option>
			</select>
			<input type="submit" value="Route" class="btn" id="submit">
		</form>

	    </div>
	  </div>
	</div>


	<br><br>