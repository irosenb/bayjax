<? 
	/********
	Isaac Rosenberg
	index.php
	Main page, shows map, handles AJAX, etc.
	********/

	//just setting up
	require_once('../includes/helpers.php'); 
	session_start();

	render('header', array('title' => 'BART index'));  
?>

<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?key=AIzaSyDUE08r9kD1p5QsqOzmI6_EcoUNCJntf5I&sensor=false"></script>
<script type="text/javascript">
	
	//start the map. code from google maps tutorial.
	function initialize() { 
        var mapOptions = {
          center: new google.maps.LatLng(37.7750, -122.4183),
          zoom: 11,
          mapTypeId: google.maps.MapTypeId.ROADMAP
        };
        
        //return a map for later use
        return new google.maps.Map(document.getElementById("map_canvas"),
            mapOptions); 
    }
    /*
     *Function request takes no parameters. 
     *It is called when the user submits the route form. 
     *request() will then go and post to stations.php, with the route value
     *and then will create the coordinates array, adding all coordinates of each marker in,
     *finally displaying the route and its markers.  	 
     */
    function request() {
    	$.ajax ({
			type: "POST",
			dataType: "json",
			url: "stations.php",
			data: {
				route: $("#route").val()
			},
			success: function(data) {
				var routePathCoords = [];
				//check if input was real
				if (data === null) {
					$("form").append("<div class='alert alert-error'><a class='close' data-dismiss='alert' href='#'>×</a><strong>Error:</strong> Route not found.</div>");
				}
				var pathColor = data.color;	

				//create coordinates from all points 
				$.each(data.stations, function(i,j) {
					routePathCoords.push(new google.maps.LatLng(data.lat[i], data.longit[i]));
				});

				//create new map
				var map = initialize();

				//generating the polyline
				var routePath = new google.maps.Polyline({ 
					path: routePathCoords,
					//set strokecolor to the route color
					strokeColor: pathColor,
					strokeOpacity: 1.0,
					strokeWeight: 5
				});

				//print path to map 
				routePath.setMap(map);

				//creating each marker, display on screen
				$.each(routePathCoords, function(i,c) {
					var marker = new google.maps.Marker({
						//obtaining from coordinates array
						position: c, 
						map: map,
						//obtaining from stations array
						title: data.stations[i] 
					});

					var fullname = data.statname[i];
					var name = marker.title;
					//listen for click, then activate window
					google.maps.event.addListener(marker, 'click', stnWindow(fullname, name, marker, map));

				});

			},

		});
		return false;
    }
    /*Function stnWindow takes parameters of fullname, name, marker, and map. 
     *It will then create a window out of the parameters for the given marker,
     *displaying it over the clicked marker.
     *These all are called from the request function. 
     */
    function stnWindow(fullname, name, marker, map) {
    	return function() {
    		$.ajax({
    	    		type: "POST",
    	    		dataType: "json",
    	    		url: "window.php",
    				data: {
    					name: name
    				},
    				success: function(data) {
    					//initializing variables for window+content
    					var station = fullname;
    					var infoWindow = new google.maps.InfoWindow();
    					//adding station title
    					var content = "<h1>" + station + "</h1>";
    					//get names and times, put into var content
    					$.each(data.destns, function(i,n) {
    						content = content + "<h2> Destination: </h2>" + n + "<h3> Arrival: </h3>" ;
    						//make text color red if text is leaving
    						if (data.arrivals[i] === "Leaving"){
    							content = content + '<p style= "color: red;">' + data.arrivals[i] + "</p>";
    						}
    						else {
    							content = content + data.arrivals[i] + " minutes";
    						}
    	
    					});
    					//print content to infowindow
    					infoWindow.setContent(content);
    					infoWindow.open(map, marker);

    					google.maps.event.addListener(map, 'click', function() {
							infoWindow.close();
						});

    				}
    	    	});

			
    	}
    }
</script>

<script type="text/javascript">
	
	

</script>

<? 
	render('header2'); 
?>

<div id="map_canvas" style="width:100%; height:95%"></div>

<? 
	render('footer'); 
?>
