<!--
	Isaac Rosenberg
	header.php
	Beginning of html document. Simple. 
-->
<!DOCTYPE html>
<html>
  <head>
    <meta name="viewport" content="width=device-width"> <!--from the 2nd lecture's source-->
    <title><?php echo htmlspecialchars($title) ?></title>
	<script src="http://code.jquery.com/jquery-latest.js"></script>
	<script type="text/javascript" src="http://netdna.bootstrapcdn.com/twitter-bootstrap/2.0.4/js/bootstrap.js"></script>
	<link href='http://twitter.github.com/bootstrap/assets/css/bootstrap.css' rel='stylesheet' />
	<style type="text/css">
		html, body 
		{
			height: 100%;
		}
	</style>