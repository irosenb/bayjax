<? 
	/********
	Isaac Rosenberg
	window.php
	Gets all the variables to be then printed in the selected marker's infoWindow.
	********/	
	require_once("../includes/helpers.php");
	//getting station name
	if (isset($_POST['name'])) {
		//escape malicious chars
		$stn = htmlspecialchars($_POST['name']);


		//load file from BART
		$xml = simplexml_load_file("http://api.bart.gov/api/etd.aspx?cmd=etd&orig={$stn}&key=2THK-XXUA-QDTE-ZNQD");
		//header type of json
		header("Content-type: application/json");

		//defining class to store variables in 
		class Time {
			//fullname of the station
			public $fullname;
			public $arrivals = array();
			//set array for destinations
			public $destns = array();
		}
		//if xml does exist
		if ($xml !== FALSE) {

			//make new class
			$time = new Time;

			foreach ($xml->xpath('//station') as $name) {

				//get times and destinations of arrival
				foreach ($name->etd as $times) {
					array_push($time->destns, (string) $times->destination);
					array_push($time->arrivals, (string) $times->estimate->minutes);
				}
			}

			
		}
		//echo results for parsing
		echo json_encode($time);
	}
?>