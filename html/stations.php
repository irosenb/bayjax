<?
	/********
	Isaac Rosenberg
	stations.php
	Queries database for route and its properties, eg. route color, station coordinates, etc.
	********/
	
	//include helpful functions
	require_once("../includes/helpers.php"); 

	//connect db
	$dbh = connect_db('mysql:host=localhost;dbname=project2', 'jharvard', 'crimson');
	
	
	
	//if route exists	
	if (isset($_POST['route'])) {
		//defining class Route
		class Route {
			//place 2 store stations
			public $stations = array();
			public $color;
			//storing latitude + longitude
			public $lat = array();
			public $longit = array();
			//storing station name 
			public $statname = array();
		}
		header("Content-type: application/json");
		
		//getting desired route
		$route = htmlspecialchars($_POST['route']); 
		//find route within database
		$query = $dbh->query("SELECT color, lat, longit, station, stnfull FROM stations WHERE route='$route'");
		
		//does query exist?
		if ($query->rowCount() > 0) { 
			$route = new Route();
			//getting all variable names
			foreach ($query as $row) { 
				//then setting them to object variables
				$route->color = $row['color']; 
				//adding on for each station to the array
				array_push($route->stations, $row['station']); 
				//adding latitude
				array_push($route->lat, $row['lat']);
				//and longitude
				array_push($route->longit, $row['longit']); 
				//top it off with statname
				array_push($route->statname, $row['stnfull']);

			}
			unset($row);
			//printing out encoded json for parsing
			echo json_encode($route);
		}
		
	}

?>